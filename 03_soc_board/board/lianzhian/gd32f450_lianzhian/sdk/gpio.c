/**
  ******************************************************************************
  * @file    gpio.c
  * @brief   This file provides code for the configuration
  *          of all used GPIO pins.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "gd32f4xx.h"

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/*----------------------------------------------------------------------------*/
/* Configure GPIO                                                             */
/*----------------------------------------------------------------------------*/
/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

#define LED1_PIN                         GPIO_PIN_4
#define LED1_GPIO_PORT                   GPIOD
#define LED1_GPIO_CLK                    RCU_GPIOD
  
#define LED2_PIN                         GPIO_PIN_5
#define LED2_GPIO_PORT                   GPIOD
#define LED2_GPIO_CLK                    RCU_GPIOD
  
#define LED3_PIN                         GPIO_PIN_3
#define LED3_GPIO_PORT                   GPIOG
#define LED3_GPIO_CLK                    RCU_GPIOG

/** Configure pins as
        * Analog
        * Input
        * Output
        * EVENT_OUT
        * EXTI
*/
void GPIO_Init(void)
{
  
}


void led_init(void)
{
	//LED 1
    /* enable the led clock */
    rcu_periph_clock_enable(RCU_GPIOD);
    /* configure led GPIO port */ 
    gpio_mode_set(GPIOD, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE,GPIO_PIN_4);
    gpio_output_options_set(GPIOD, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ,GPIO_PIN_4);

	
	//LED 2
    /* enable the led clock */
    rcu_periph_clock_enable(RCU_GPIOD);
    /* configure led GPIO port */ 
    gpio_mode_set(GPIOD, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE,GPIO_PIN_5);
    gpio_output_options_set(GPIOD, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ,GPIO_PIN_5);

	
	//LED 3
    /* enable the led clock */
    rcu_periph_clock_enable(RCU_GPIOG);
    /* configure led GPIO port */ 
    gpio_mode_set(GPIOG, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE,GPIO_PIN_3);
    gpio_output_options_set(GPIOG, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ,GPIO_PIN_3);
}



void led_on(int lednum)
{
	switch(lednum)
	{
		case 1:
    		gpio_bit_set(GPIOD,GPIO_PIN_4);
			break;
			
		case 2:
    		gpio_bit_set(GPIOD,GPIO_PIN_5);
			break;
			
		case 3:
    		gpio_bit_set(GPIOG,GPIO_PIN_3);
			break;
	}
}


void led_off(int lednum)
{
	switch(lednum)
    {
		case 1:
    		gpio_bit_reset(GPIOD,GPIO_PIN_4);
			break;
			
		case 2:
    		gpio_bit_reset(GPIOD,GPIO_PIN_5);
			break;
			
		case 3:
    		gpio_bit_reset(GPIOG,GPIO_PIN_3);
			break;
	}
}

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
